@extends('layouts.website')

@section('title')
VinumDay anniversary: UI design and Motion.
@endsection


@section('description')
Animated wrapped page to celebrate company's anniversary.
@endsection


@section('content')
    <div class="container text-container">
        <h1>VinumDay anniversary: UI design and Motion.</h1>
        <p>Animated wrapped page to celebrate company's anniversary.</p>
    </div>
    <div class="img-container">
        <img src="img/posts/img6-1.png" alt="VinumDay">
    </div>
    <div class="container text-container">
        <h3>About the project</h3>
        <p>The briefing was audacious: to create an animated "wrapped" page for the oldest clients to celebrate the company's anniversary using some curiosities about the brand's story. For first we created the content and storytelling, then we started wireframing and aligning with the client. The next step was to create the high fidelity prototypes and applying animation to it. This is the final result, use the right arrow to navigate throught the screens:</p>
    </div>
    
    <div class="img-container">
        <iframe class="screen-anniv" style="border: 1px solid rgba(0, 0, 0, 0.1);" width="1200" height="700" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Fproto%2FTezTXb66Dr97nKbWao2bKA%2FGab-Porfolio%3Fpage-id%3D339%253A179%26node-id%3D339%253A4276%26viewport%3D297%252C48%252C0.08%26scaling%3Dscale-down-width" allowfullscreen></iframe>
    </div>

    <div class="container text-container margin-top">
        <p>Hope you’ve enjoyed it, thanks for reading ❤️</p>
    </div>
@endsection