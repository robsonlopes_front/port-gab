@extends('layouts.website')

@section('title')
Improving Caixa's internet banking flow.
@endsection


@section('description')
    A convertion rate optimization (CRO) job done for a Brazilian language immersion company called The Fools. This study includes researches, data analysis, interviews, prototypes and A/B tests.
@endsection


@section('content')
    <div class="container text-container">
        <h1>Improving Caixa's internet banking flow with UX process.</h1>
        <p>This is a study case of a project I created for training the conduction of a UX flow.</p>
    </div>
    <div class="img-container">
        <img src="img/posts/img1-1.png" alt="Caixa">
    </div>
    <div class="container text-container">
        <h3>About the project</h3>
        <p>In this example, we needed a simple solution for a payment flow on a bank app. I started creating some different personas that would have to execute a simple payment flow in the app. Then, after analysing the insights from the tests, I proposed some interface and usability changes in the flow and started another test cycle. </p>
        <p>This project was written in portuguese, tell me if you need a translation. Use the right arrow to navigate.</p>
    </div>
    
    <div class="img-container">
        <iframe class="mobile-prot" style="border: 1px solid rgba(0, 0, 0, 0.1);" width="1200" height="750" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Fproto%2FmFJwfPsqqOCefYULy5X7pS%2FApresenta%C3%A7%C3%A3o-de-melhorias-no-fluxo-do-Internet-Banking-da-Caixa%3Fpage-id%3D60%253A0%26node-id%3D60%253A1%26starting-point-node-id%3D60%253A1%26scaling%3Dscale-down-width" allowfullscreen></iframe>
    </div>

    <div class="container text-container margin-top">
        <p>Hope you’ve enjoyed it, thanks for reading ❤️</p>
    </div>
@endsection