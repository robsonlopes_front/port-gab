@extends('layouts.website')

@section('title')
Unisinos: Distance educational app.
@endsection


@section('description')
Mobile app for distance education of one of the biggest colleges of Rio Grande do Sul.
@endsection


@section('content')
    <div class="container text-container">
        <h1>Unisinos: Distance educational app.</h1>
        <p>Mobile app for distance education of one of the biggest colleges of Rio Grande do Sul.</p>
    </div>
    <div class="img-container">
        <img src="img/posts/img3-1.png" alt="Unisinos">
    </div>
    <div class="container text-container">
        <h3>About the project</h3>
        <p>Before starting this project, there were some briefing meetings with the client, user flow definition and wireframe creation. Then, this was the final result:</p>
    </div>
    
    <div class="img-container">
        <img src="img/posts/img3-2.png" alt="Unisinos">
    </div>

    <div class="container text-container margin-top">
        <p>Hope you’ve enjoyed it, thanks for reading ❤️</p>
    </div>
@endsection