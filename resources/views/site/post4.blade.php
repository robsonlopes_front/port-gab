@extends('layouts.website')

@section('title')
Elevator on your hands: an app to call the elevator.
@endsection


@section('description')
A mobile app created to remotly call an elevator on during the pandemic.
@endsection


@section('content')
    <div class="container text-container">
        <h1>Elevator on your hands: an app to call the elevator.</h1>
        <p>A mobile app created to remotly call an elevator on during the pandemic.</p>
    </div>
    <div class="img-container">
        <img src="img/posts/img4-1.png" alt="TKE">
    </div>
    <div class="container text-container">
        <h3>About the project</h3>
        <p>The first step was defining the user flow, considering the complexity of business rules that the brand demanded. We divided the flows in sprints and started creating the wireframes. After that, we even designed a high fidelity prototype and tested with a future real user. This are some of the wireframes: </p>
    </div>
    
    <div class="img-container">
        <img src="img/posts/img4-2.png" alt="TKE">
    </div>

    <div class="container text-container margin-top">
        <p>Hope you’ve enjoyed it, thanks for reading ❤️</p>
    </div>
@endsection