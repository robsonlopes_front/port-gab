@extends('layouts.website')

@section('title')
    Gabriel Moreira - UX Designer
@endsection


@section('description')
I’m a UI/UX Designer based in Brazil. I work for 3 years creating digital products, apps and ecommerces focused on convertion and usabillity.
@endsection


@section('content')
    <div class="container">
        <div class="hello-home">
            <div class="left">
                <img src="img/star.png" alt="Star" width="40" class="star star1">
                <img src="img/star.png" alt="Star" width="36" class="star star2">
                <img src="img/star.png" alt="Star" width="39" class="star star3">
                <img src="img/star.png" alt="Star" width="39" class="star star4">

                <h1 class="desktop">Hey there. I'm <strong class="special">Gabriel</strong> <img src="img/rainbow.png" alt="Hi" class="rainbow"></h1>
                <h1 class="mobile">Hey there. <br>I'm <strong class="special">Gabriel</strong> <img src="img/rainbow.png" alt="Hi" class="rainbow"></h1>
                <h2>For a good experience, you can count on me!</h2>

                <div class="texts">
                    <p>I’m a UI/UX Designer based in Brazil. I work for 3 years creating digital products, apps and ecommerces focused on convertion and usabillity.</p>
                </div>

                <button type="button" class="btn-action">Know my work</button>
            </div>
        </div>

        <div class="about-me">
            <div class="left">
                <h2>About me</h2>
                <p>I’m <strong>22 years</strong> old and currently living in Canoas - RS. <br><br>I’m very curious and always willing to help the others. My professional focus is to democratize <strong>good experiences</strong> to every possible user. During the weekends I love to play videogames and read books.</p>

                <ul class="skill-list">
                    <li>UX/UI</li>
                    <li>Figma</li>
                    <li>Design System</li>
                    <li>HTML, CSS and JS</li>
                    <li>Usability tests</li>
                    <li>UI Patterns</li>
                    <li>Photoshop and Illustrator</li>
                    <li>Mobile Apps</li>
                    <li>Ecommerces</li>
                    <li>Desktop Products</li>
                    <li>Motion</li>
                    <li>Prototyping</li>
                    <li>User flows</li>
                    <li>Personas</li>
                </ul>
            </div>
            <div class="right">
                <img src="img/about-me.png" alt="About me">
            </div>
        </div>

        <div class="work-list">
            <h2>My work</h2>

            <div class="list-blocks">
                <div class="item" data-cat="ux-research">
                    <a href="/improving-caixas-internet-banking-flow" title="Improving Caixa's internet banking flow.">
                        <img src="img/thumbs/thumb-1.png" alt="Improving Caixa's internet banking flow.">
                        <h4>Improving Caixa's internet banking flow.</h4>
                    </a>
                </div>
                <div class="item" data-cat="product-design,visual-ui-design">
                    <a href="/vinumday-a-new-look-feel-focused-on-convertion" title="VinumDay: a new look & feel focused on convertion.">
                        <img src="img/thumbs/thumb-2.png" alt="VinumDay: a new look & feel focused on convertion.">
                        <h4>VinumDay: a new look & feel focused on convertion.</h4>
                    </a>
                </div>
                <div class="item" data-cat="visual-ui-design">
                    <a href="/unisinos-distance-educational-app" title="Unisinos: Distance educational app.">
                        <img src="img/thumbs/thumb-3.png" alt="Unisinos: Distance educational app.">
                        <h4>Unisinos: Distance educational app.</h4>
                    </a>
                </div>
                <div class="item" data-cat="motion-design">
                    <a href="/elevator-on-your-hands-an-app-to-call-the-elevator" title="Elevator on your hands: an app to call the elevator.">
                        <img src="img/thumbs/thumb-4.png" alt="Elevator on your hands: an app to call the elevator.">
                        <h4>Elevator on your hands: an app to call the elevator.</h4>
                    </a>
                </div>
                <div class="item" data-cat="visual-ui-design">
                    <a href="/professional-page-on-metacem-a-digital-visit-card" title="Professional page on Metacem: a digital visit card.">
                        <img src="img/thumbs/thumb-5.png" alt="Professional page on Metacem: a digital visit card.">
                        <h4>Professional page on Metacem: a digital visit card.</h4>
                    </a>
                </div>
                <div class="item" data-cat="ux-analysis">
                    <a href="/vinumday-anniversary-ui-design-and-motion" title="VinumDay anniversary: UI design and Motion.">
                        <img src="img/thumbs/thumb-6.png" alt="VinumDay anniversary: UI design and Motion.">
                        <h4>VinumDay anniversary: UI design and Motion.</h4>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection