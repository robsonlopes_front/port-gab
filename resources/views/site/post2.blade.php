@extends('layouts.website')

@section('title')
VinumDay: a new look & feel focused on convertion.
@endsection


@section('description')
Creating good solutions from the wine fans!
@endsection


@section('content')
    <div class="container text-container">
        <h1>VinumDay: a new look & feel focused on convertion.</h1>
        <p>Creating good solutions from the wine fans!</p>
    </div>
    <div class="img-container">
        <img src="img/posts/img2-1.png" alt="VinumDay">
    </div>
    <div class="container text-container">
        <h3>About the project</h3>
        <p>This wine ecommerce has a different special offer every day. I needed to improve the user's experience focusing in convertion and sales. </p>
        <p>This project was written in portuguese, tell me if you need a translation.</p>
    </div>
    
    <div class="img-container">
        <iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="1200" height="550" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Fproto%2FTezTXb66Dr97nKbWao2bKA%2FGab-Porfolio%3Fpage-id%3D108%253A3077%26node-id%3D328%253A3285%26viewport%3D297%252C48%252C0.22%26scaling%3Dscale-down-width" allowfullscreen></iframe>
    </div>

    <div class="container text-container margin-top">
        <p>Hope you’ve enjoyed it, thanks for reading ❤️</p>
    </div>
@endsection