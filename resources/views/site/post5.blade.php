@extends('layouts.website')

@section('title')
Professional page on Metacem: a digital visit card.
@endsection


@section('description')
Metacem is a telemedicine online portal and this is the new professional page of the users (doctors).
@endsection


@section('content')
    <div class="container text-container">
        <h1>Professional page on Metacem: a digital visit card.</h1>
        <p>Metacem is a telemedicine online portal and this is the new professional page of the users (doctors).</p>
    </div>
    <div class="img-container">
        <img src="img/posts/img5-1.png" alt="Metacem">
    </div>
    <div class="container text-container">
        <h3>About the project</h3>
        <p>This page acts like a online visit card so that their clients can find the platform. On this page, the clients can access the doctor's social medias, see the clinic's photos and have more informations about the doctors and get in contact.</p>
    </div>
    
    <div class="img-container">
        <iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="1200" height="550" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Fproto%2FTezTXb66Dr97nKbWao2bKA%2FGab-Porfolio%3Fpage-id%3D108%253A3686%26node-id%3D328%253A2914%26viewport%3D297%252C48%252C0.32%26scaling%3Dscale-down-width" allowfullscreen></iframe>
    </div>

    <div class="container text-container margin-top">
        <p>Hope you’ve enjoyed it, thanks for reading ❤️</p>
    </div>
@endsection