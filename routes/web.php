<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'guest'], function(){;

    Route::get('/', 'HomeController@index')->name('home');
    
    Route::get('/improving-caixas-internet-banking-flow', 'HomeController@post1')->name('post1');
    Route::get('/vinumday-a-new-look-feel-focused-on-convertion', 'HomeController@post2')->name('post2');
    Route::get('/unisinos-distance-educational-app', 'HomeController@post3')->name('post3');
    Route::get('/elevator-on-your-hands-an-app-to-call-the-elevator', 'HomeController@post4')->name('post4');
    Route::get('/professional-page-on-metacem-a-digital-visit-card', 'HomeController@post5')->name('post5');
    Route::get('/vinumday-anniversary-ui-design-and-motion', 'HomeController@post6')->name('post6');

});